var express = require('express');
var ejs = require('ejs');
var path = require('path');
var app = express();

app.set("view engine", "ejs");
app.engine('html', ejs.renderFile);
app.use(express.static(path.join(__dirname, 'public')));

app.get("/", function(request, response){ 
    response.render('index.html');
});

app.listen(3000);
